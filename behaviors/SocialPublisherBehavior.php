<?php
namespace emilasp\publisher\behaviors;

use emilasp\core\helpers\StringHelper;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Поведение добавляет модели тегирование
 *
 * Добавляем поведение:
 * ```php
 * 'tags'  => [
 *     'class' => PublisherBehavior::className(),
 * ],
 * ```
 * Class SocialPublisherBehavior
 * @package emilasp\taxonomy\behaviors
 */
class SocialPublisherBehavior extends Behavior
{
    /*public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveTags',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveTags',
        ];
    }*/

    /**
     * Сохраняем теги для модели
     */
    public function saveTags()
    {
        $model = $this->owner;
        $tags  = $this->formTags;

        $tags = explode(',', $tags);

        $issetTags = [];
        foreach ($tags as $tag) {
            if ($tag) {
                $tagInBd                 = $this->getTag($tag);
                $issetTags[$tagInBd->id] = $tagInBd;
            }
        }

        foreach ($model->{$this->relation} as $issetTag) {
            if (!isset($issetTags[$issetTag->id])) {
                $linkToDelete = TagLink::findOne([
                    'object'    => $model::className(),
                    'object_id' => $model->id,
                    'tag_id'    => $issetTag->id,
                ]);

                if ($linkToDelete) {
                    $linkToDelete->delete();
                }
            }
        }
    }

    /**
     * Получаем tag
     *
     * @param $tag
     * @return Tag|null
     */
    private function getTag($tag)
    {
        $model = $this->owner;
        $tagModelName = $this->modelName;

        $tagInBd = null;
        foreach ($model->{$this->relation} as $issetTag) {
            if ($issetTag->name === $tag) {
                $tagInBd = $issetTag;
                break;
            }
        }

        if (!$tagInBd) {
            $tagInBd = $tagModelName::findOne(['name' => $tag]);

            if (!$tagInBd) {
                $tagInBd = new $tagModelName(['name' => $tag, 'slug' => StringHelper::str2url($tag), 'status' => 1]);
                $tagInBd->save();
            }

            $tagLink            = new TagLink();
            $tagLink->object    = $model::className();
            $tagLink->object_id = $model->id;
            $tagLink->tag_id    = $tagInBd->id;
            $tagLink->save();
        }

        return $tagInBd;
    }

    /**
     * Устанавливаем теги для модели
     */
    public function setTags()
    {
        $tags = $this->owner->tags;

        if ($tags) {
            $this->formTags = implode(',', ArrayHelper::map($tags, 'id', 'name'));
        }
    }
}
